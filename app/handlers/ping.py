from app.handlers.base import RequestHandler
from tornado.web import asynchronous
from tornado.gen import coroutine
from tornado.concurrent import run_on_executor
from concurrent.futures import ThreadPoolExecutor

class PingHandler(RequestHandler):
    executor = ThreadPoolExecutor(64) 

    @asynchronous
    @coroutine
    def get(self):
        self.write("pong!")
